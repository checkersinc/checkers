package com.checkers.utils;

public enum Colour {
    WHITE,
    BLACK
}
