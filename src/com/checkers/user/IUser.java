package com.checkers.user;

public interface IUser {
    void makeTurn(String from, String to) throws Exception;
}
